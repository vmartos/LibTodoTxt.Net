# LibTodoTxt.Net

.NetStandard library to parse, modify and add new tasks to Todo.txt files (see: https://github.com/todotxt/todo.txt)